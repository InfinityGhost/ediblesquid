# Edible Squid
[![pipeline status](https://gitlab.com/1Megu/ediblesquid/badges/1.15.2/pipeline.svg)](https://gitlab.com/1Megu/ediblesquid/-/commits/1.15.2)
[![Crowdin](https://badges.crowdin.net/ediblesquid/localized.svg)](https://crowdin.com/project/ediblesquid)
[![Discord](https://img.shields.io/discord/711343231207735306?color=7289DA&label=Discord&logo=Discord)](https://discord.gg/xr3ckqv)
[![CurseForge Downloads](https://img.shields.io/badge/dynamic/json?color=success&label=downloads&query=downloadCount&url=https%3A%2F%2Faddons-ecs.forgesvc.net%2Fapi%2Fv2%2Faddon%2F383926)](https://www.curseforge.com/minecraft/mc-mods/edible-squid)

A fairly simple Fabric mod to give Squid some more usefulness. Adds raw squid as a drop and fried calamari for better nutrition. Should be compatible with pretty much any other food mod.

