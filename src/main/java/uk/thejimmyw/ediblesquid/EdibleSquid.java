package uk.thejimmyw.ediblesquid;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.loot.v1.FabricLootPoolBuilder;
import net.fabricmc.fabric.api.loot.v1.event.LootTableLoadingCallback;
import net.minecraft.item.FoodComponent;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.loot.ConstantLootTableRange;
import net.minecraft.loot.entry.ItemEntry;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class EdibleSquid implements ModInitializer
{
    public static final RawSquid RAW_SQUID = new RawSquid(new Item.Settings().group(ItemGroup.FOOD).food(new FoodComponent.Builder().hunger(2).saturationModifier(0.2f).build()));
    public static final CookedSquid COOKED_SQUID = new CookedSquid(new Item.Settings().group(ItemGroup.FOOD).food(new FoodComponent.Builder().hunger(5).saturationModifier(1.2f).build()));

    private static final Identifier SQUID_LOOT_TABLE_ID = new Identifier("minecraft", "entities/squid");

    @Override
    public void onInitialize()
    {
        Registry.register(Registry.ITEM, new Identifier("ediblesquid", "raw_squid"), RAW_SQUID);
        Registry.register(Registry.ITEM, new Identifier("ediblesquid", "cooked_squid"), COOKED_SQUID);

        LootTableLoadingCallback.EVENT.register((resourceManager, lootManager, id, supplier, setter) -> {
            if (SQUID_LOOT_TABLE_ID.equals(id)) {
                FabricLootPoolBuilder poolBuilder = FabricLootPoolBuilder.builder()
                        .withRolls(ConstantLootTableRange.create(1))
                        .withEntry(ItemEntry.builder(RAW_SQUID));

                supplier.withPool(poolBuilder);
            }
        });
    }
}
